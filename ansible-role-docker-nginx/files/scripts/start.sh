#!/bin/bash

##############################
# Variables
##############################

email=${EMAIL_TO}
xargs="${EXTRA_ARGS}"

##############################
# Start receiving certs
##############################

cd /etc/nginx/conf.d/

for conf in `ls -1`; do

  [[ -z `grep server_name ${conf}` ]] && continue

  dom=`grep server_name ${conf} \
    | awk -F' ' '{print $2}' \
    | sed 's/;//g' \
    | sort -u`

  testcert="--test-cert"
  if [[ -z `grep -- ${testcert} ${conf}` ]]; then
    testcert=""
  fi

  if [[ ${dom} == "localhost" ]]; then
    continue
  fi

  if [[ -d /etc/letsencrypt/live/${dom} ]]; then
    echo "Certificate for ${dom} already received."
    continue
  fi

  set -x
  certbot certonly -n --standalone \
    ${xargs} ${testcert} -d ${dom} \
    -m ${email} \
    --agree-tos
  set +x

  [[ $? != 0 ]] && exit 1

done

##############################
# Start Nginx
##############################

counter=0
while true; do

  # Check renew certificate every 10sec*
  if [[ ${counter} -ge 10000 ]] || \
     [[ ${counter} -eq 0 ]]; then
    service nginx stop
    certbot renew
    counter=1
  fi

  service nginx status
  [[ $? != 0 ]] && service nginx start
  sleep 10

  let counter=${counter}+1
done



