
---

- name: Setting hostname
  hostname:
    name: prometheus

- name: Creating user prometheus
  user:
    name: prometheus
    comment: Prometheus Monitoring

- name: Installing packages
  package:
    name:
      - tar
    state: latest

- name: Downloading prometheus
  get_url:
    url: "{{ prometheus_url }}/v{{ prometheus_version }}/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz"
    dest: "/root/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz"
    checksum: sha256:5fcc35b78bd0a1b84afae6de94248a4bea3cdb4daf0d54a37b5491cb86b014d7

- name: Unpacking archive
  unarchive:
    src: "/root/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz"
    dest: "/opt/"
    remote_src: yes

- name: Renaming Prometheus folder
  shell: | 
    rm -rf /opt/prometheus
    mv /opt/prometheus-{{ prometheus_version }}.linux-amd64 \
       /opt/prometheus

- name: Change file ownership, group and permissions
  file:
    path: "/opt/prometheus"
    owner: root
    group: root
    recurse: yes

- name: Copying Prometheus systemd service file
  template:
    src: "{{ item.src }}.j2"
    dest: "{{ item.dest }}"
    mode: '0644'
    owner: root
    group: root
  with_items:
    - { src: "prometheus.service", dest: "/etc/systemd/system/prometheus.service" }
    - { src: "prometheus.yml",     dest: "/opt/prometheus/prometheus.yml" }

- name: Ensuring service is started
  systemd:
    name: prometheus.service
    state: started
    daemon_reload: yes
    enabled: yes

- name: Copying nginx reverse proxy configuration
  template:
    src: templates/prometheus.conf.j2
    dest: /root/prometheus.conf

- name: Fetching generated nginx configuration
  fetch:
    src: /root/prometheus.conf
    dest: /etc/nginx/conf.d/prometheus.conf
    flat: yes
  notify:
    - Restart Nginx

- name: Ensuring firewall ports are open
  firewalld:
    port: "{{ item }}"
    permanent: yes
    state: enabled
  with_items:
    - "9090/tcp"

- name: Reloading service firewalld
  systemd:
    name: firewalld
    state: reloaded

- import_tasks: node_exporter.yml

